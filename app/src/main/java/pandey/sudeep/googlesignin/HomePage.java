package pandey.sudeep.googlesignin;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class HomePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        Intent intent = getIntent();

        TextView name = (TextView)findViewById(R.id.name);
        TextView givenName = (TextView)findViewById(R.id.givenName);
        TextView familyName = (TextView)findViewById(R.id.familyName);
        TextView userEmail = (TextView)findViewById(R.id.email);
        TextView Id = (TextView)findViewById(R.id.Id);
        ImageView imageView = (ImageView)findViewById(R.id.profilePic);

        name.setText(intent.getExtras().getString("name"));
        givenName.setText(intent.getExtras().getString("givenName"));
        familyName.setText(intent.getExtras().getString("familyName"));
        userEmail.setText(intent.getExtras().getString("email"));
        Id.setText(intent.getExtras().getString("Id"));
        imageView.setImageURI(Uri.parse(intent.getExtras().getString("photoURI")));
        //imageView.setImageResource(R.drawable.car);

        Glide.with(this).load(intent.getExtras().getString("photoURI")).into(imageView);


    }
}
